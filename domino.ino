
#include <TFT.h>
#include <SPI.h>

#define cs   7
#define dc   0
#define rst  1

// buttons
const int buttonDownPin = 2;
const int buttonUpPin = 3;

// layout
const int dY=12;
const int dX=20;

// theme
const int LIGHT[3] = {104, 122, 164};
const int MEDIUM[3] = {68, 89, 138};
const int DARK[3] = {18, 38, 84};

// config
char* principale[] = {"Antifurto", "Ambienti", "Punti luce", "Tapparelle e lucernari", "Programmi", "Configurazione"};
char* zone[] = {"Zona giorno", "Zona notte", "Bagni", "Balconi e altro"};


TFT tft = TFT(cs, dc, rst);
int index=0;

void setup() {
  
  // buttons setup
  pinMode(buttonDownPin, INPUT);
  pinMode(buttonUpPin, INPUT);
  
  // tft setup
  tft.begin();
  tft.setTextSize(1);
  tft.background(DARK[0], DARK[1], DARK[2]);

  // main manu
  menu(principale, 0, 0);

}

void loop() {

  int direction = 0;
  while( direction==0 ){
    if( digitalRead(buttonDownPin)!=0 ){
      direction = 1;
      break;
    }
    if( digitalRead(buttonUpPin)!=0 ){
      direction = -1;
      break;
    }
  }
  if( direction!=0 ){
    menu(principale, 0, direction);
    delay(100);
  }
  
}

void menu(char* strings[], int level, int delta) {
  bool refresh = delta==0; // prima apertura del men -> refresh
  int n = 6; // TODO: dimensione dell'array
  int t = index+delta;
  if( t<0 ){
    t += n;
  }
  int newIndex = t%n;
  int x = level*dX;
  tft.noFill();
  tft.stroke(LIGHT[0], LIGHT[1], LIGHT[2]);
  tft.rect(0, 0, 159, 127);
  for(int i=0; i<n; i++){
    int y=dY*i+6;
    if( i==newIndex ){
      tft.stroke(MEDIUM[0], MEDIUM[1], MEDIUM[2]);
      tft.fill(MEDIUM[0], MEDIUM[1], MEDIUM[2]);
      tft.rect(x+1, y-2, 157, 12);
      tft.stroke(DARK[0], DARK[1], DARK[2]);
      tft.text(strings[i], x+6, y);
    } else if( i==index || refresh ){
      tft.stroke(DARK[0], DARK[1], DARK[2]);
      tft.fill(DARK[0], DARK[1], DARK[2]);
      tft.rect(x+1, y-2, 157, 12);
      tft.stroke(LIGHT[0], LIGHT[1], LIGHT[2]);
      tft.text(strings[i], x+6, y);
    }
  }
  index = newIndex;
}
  
